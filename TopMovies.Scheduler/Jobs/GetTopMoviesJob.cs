﻿using Quartz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TopMovies.Scheduler.Jobs
{
    public class GetTopMoviesJob : IJob
    {
        private readonly ILogger<GetTopMoviesJob> _logger=null;

        public GetTopMoviesJob()
        {

        }
        public GetTopMoviesJob(ILogger<GetTopMoviesJob> logger)
        {
            _logger = logger;
        }
        public async Task Execute(IJobExecutionContext context)
        {
            _logger?.LogInformation("GetTopMovies - Start");
            var httpClient = new HttpClient();
            var client = new TopMoviesClient("https://localhost:7003/", httpClient);
            var result = await client.GetTopMoviesAsync(210);
            _logger?.LogInformation("GetTopMovies - Stop");
        }
    }
}
