using Quartz;
using TopMovies.Scheduler;
using TopMovies.Scheduler.Jobs;

IHost host = Host.CreateDefaultBuilder(args)
    .ConfigureServices((hostContext, services) =>
    {
        //services.AddHostedService<Worker>();
        services.AddTransient<GetTopMoviesJob>();

        services.AddQuartz(q =>
        {
            // base quartz scheduler, job and trigger configuration
            
            var job=JobBuilder.Create<GetTopMoviesJob>().WithIdentity("Get Movies Job", "Get group").Build();
            var cron = hostContext.Configuration.GetSection("Quartz").GetValue<string>("GetTopMoviesCron");
            q.AddJob<GetTopMoviesJob>(job.Key);

            q.AddTrigger(t => t
            .WithIdentity("Get Movies Trigger")
            .ForJob(job.Key)
            .StartAt(DateBuilder.EvenSecondDate(DateTimeOffset.UtcNow.Date))
            .WithCronSchedule(cron)
            .WithDescription("Get top movies trigger")
        );
        });

        services.AddQuartzHostedService(q => q.WaitForJobsToComplete = true);
    })
    .Build();

await host.RunAsync();
