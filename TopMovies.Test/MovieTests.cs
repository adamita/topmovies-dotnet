using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using TopMovies.BLL;
using TopMovies.Controllers;
using TopMovies.DAL;

namespace TopMovies.Test
{
    [TestClass]
    public class MovieTests
    {
        [TestMethod]
        public async Task GetMoviesTest()
        {
            // Arrange
            var logger = Mock.Of<ILogger<MovieController>>();
            var repo = new MovieRepository(new DAL.Model.TopMoviesContext());
            var tmdb = new TMDBRepository();
            var controller = new MovieController(logger, repo, tmdb);

            // Act
            var response = await controller.GetAsync(210);
            Assert.IsTrue(response);
        }
    }
}