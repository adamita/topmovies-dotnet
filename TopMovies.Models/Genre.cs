﻿using TopMovies.Interfaces;

namespace TopMovies.Models
{
    public class Genre : IGenre
    {
        public long Id { get; set; }
        public string? Name { get; set; }
    }
}
