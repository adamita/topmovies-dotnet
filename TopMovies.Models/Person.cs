﻿using TopMovies.Interfaces;

namespace TopMovies.Models
{
    public class Person : IPerson
    {
        public long Id { get; set; }
        public string? Name { get; set; }
        public string? Bio { get; set; }
        public DateTime? Birthday { get; set; }
    }
}