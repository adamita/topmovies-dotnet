﻿using TopMovies.Interfaces;

namespace TopMovies.Models
{
    public class Movie : IMovie
    {
        public long Id { get; set; }
        public string? Url { get; set; }
        public string? Title { get; set; }
        public string? Overview { get; set; }
        public int? Length { get; set; }
        public string? Poster_url { get; set; }
        public double? Vote_average { get; set; }
        public int? Vote_count { get; set; }
        public IEnumerable<IGenre> Genres { get; set; } = new List<IGenre>();
        public DateTime? ReleaseDate { get; set; }
        public IPerson? Director { get; set; } = null;
    }
}
