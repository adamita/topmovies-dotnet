﻿CREATE TABLE [dbo].[Movie]
(
	[Id] BIGINT NOT NULL PRIMARY KEY, 
    [Title] NVARCHAR(1024) NULL, 
    [Overview] NVARCHAR(2048) NULL, 
    [Url] NVARCHAR(1200) NULL, 
    [Length] INT NULL, 
    [Poster_url] NVARCHAR(256) NULL, 
    [Vote_varage] DECIMAL(3, 1) NULL, 
    [ReleaseDate] DATETIME NULL, 
    [DirectorId] BIGINT NULL, 
    [Created] DATETIME NOT NULL DEFAULT GETDATE(), 
    [Updated] DATETIME NOT NULL DEFAULT GETDATE(), 
    [Removed] BIT NOT NULL DEFAULT 0
    CONSTRAINT [FK_Movie_Person_Id] FOREIGN KEY ([DirectorId]) REFERENCES [Person]([Id])
)
