﻿
CREATE VIEW TopMoviesList as
select
	h.[Id] as HistoryId
	,h.[Date]
	,tm.[Order] as OrderBy
	,m.[Title]
    ,m.[Overview]
    ,m.[Url]
    ,m.[Length]
    ,m.[Poster_url]
    ,m.[Vote_varage]
    ,m.[ReleaseDate]
	,(select STUFF((select ', ' +  TRIM(g.Name)
		from Genre g
		join MovieGenres mg on g.Id=mg.GenreId and mg.MovieId=m.Id
        FOR XML PATH('')), 1, 1, '')) as Genres
    ,m.[DirectorId]
	,p.[Name]
    ,p.[Bio]
    ,p.[Birthday]
from TopMovieHistory h
join TopMovie tm on h.Id = tm.HistoryId and tm.Removed=0
join Movie m on m.Id=tm.MovieId and m.Removed=0
join Person p on m.DirectorId=p.Id and p.Removed=0
--order by h.[Date] desc, tm.[Order] 