﻿CREATE TABLE [dbo].[TopMovie]
(
	[Id] BIGINT NOT NULL IDENTITY(1,1) PRIMARY KEY, 
    [HistoryId] BIGINT NOT NULL, 
    [MovieId] BIGINT NOT NULL, 
    [Order] INT NOT NULL,
    [Removed] BIT NOT NULL DEFAULT 0
    CONSTRAINT [FK_TopMovie_TopMovieHistory_Id] FOREIGN KEY ([HistoryId]) REFERENCES [TopMovieHistory]([Id]),
    CONSTRAINT [FK_TopMovie_Movie_Id] FOREIGN KEY ([MovieId]) REFERENCES [Movie]([Id])
)
