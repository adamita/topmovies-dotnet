﻿CREATE TABLE [dbo].[MovieGenres]
(
	[Id] BIGINT NOT NULL IDENTITY(1,1) PRIMARY KEY, 
    [MovieId] BIGINT NOT NULL, 
    [GenreId] BIGINT NOT NULL,
    [Removed] BIT NOT NULL DEFAULT 0
    CONSTRAINT [FK_MovieGenres_Movie_Id] FOREIGN KEY ([MovieId]) REFERENCES [Movie]([Id]),
    CONSTRAINT [FK_MovieGenres_Genre_Id] FOREIGN KEY ([GenreId]) REFERENCES [Genre]([Id])
)
