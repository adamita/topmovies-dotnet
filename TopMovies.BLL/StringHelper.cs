﻿using System.Text.RegularExpressions;

namespace TopMovies.BLL
{
    public static class StringHelper
    {        public static string RemSpecChars(this string str)
        {
            return Regex.Replace(str, "[^a-zA-Z0-9_ .]+", "", RegexOptions.Compiled);
        }
    }
}