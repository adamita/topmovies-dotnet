﻿using TopMovies.BLL.TMDBService;
using TopMovies.Models;

namespace TopMovies.BLL
{
    public interface ITMDBRepository
    {
        Task<ICollection<Movie>> GetMovieDetails(string key, TMDBClient client, List<MovieListObject> list);
        Task<Movie> GetMovieDetails(string key, TMDBClient client, MovieListObject item);
        Task<List<MovieListObject>> GetTopRatedMovies(int count, string key, TMDBClient client);
    }
}