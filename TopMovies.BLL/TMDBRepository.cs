﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TopMovies.BLL.TMDBService;

namespace TopMovies.BLL
{
    public class TMDBRepository : ITMDBRepository
    {
        public TMDBRepository()
        {

        }

        public async Task<ICollection<Models.Movie>> GetMovieDetails(string key, TMDBClient client, List<MovieListObject> list)
        {
            var movies = list.AsParallel().Select(async movie => await GetMovieDetails(key, client, movie)).ToList();
            return await Task.WhenAll(movies);
        }

        public async Task<Models.Movie> GetMovieDetails(string key, TMDBClient client, MovieListObject item)
        {
            Response126 details = null;
            Response40 director = null;

            if (item.Id.HasValue)
            {
                details = await client.MovieAsync(key, item.Id.Value);
                var cast = await client.MovieCreditsAsync(key, item.Id.Value);
                var dirId = cast?.Crew?.FirstOrDefault(x => x.Job == "Director")?.Id;
                director = dirId.HasValue ? await client.PersonAsync(key, dirId.Value) : null;
            }

            return new Models.Movie()
            {
                Id = item.Id.Value,
                Title = item.Title,
                Vote_count = item.Vote_count,
                Vote_average = item.Vote_average,
                Overview = item.Overview,
                Length = details?.Runtime,
                Poster_url = details?.Poster_path,
                Genres = details?.Genres.Where(x => x.Id.HasValue).Select(x => new Models.Genre { Id = x.Id.Value, Name = x.Name }).ToList(),
                Url = $"www.themoviedb.org/movie/{item.Id}-{item.Title.RemSpecChars().Replace(' ', '-')}",
                ReleaseDate = details?.Release_date.HasValue == true ? details.Release_date.Value.UtcDateTime : null,
                Director = new Models.Person()
                {
                    Name = director?.Name,
                    Id = (long)(director?.Id.HasValue == true ? director.Id.Value : 0),
                    Bio = director?.Biography,
                    Birthday = DateTime.TryParse(director?.Birthday, out DateTime res) ? res : null
                }
            };
        }

        public async Task<List<MovieListObject>> GetTopRatedMovies(int count, string key, TMDBClient client)
        {
            var list = new List<MovieListObject>();
            Response58 resp;
            int page = 1;
            do
            {
                resp = await client.MovieTopRatedAsync(key, "en-EN", page);
                if (resp != null)
                    list.AddRange(resp.Results);
                page++;
            }
            while (list.Count < count && list.Count < resp?.Total_results);
            list = list.Take(count).ToList();
            return list;
        }
    }
}
