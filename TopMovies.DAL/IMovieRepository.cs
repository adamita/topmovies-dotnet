﻿using TopMovies.DAL.Model;
using TopMovies.Interfaces;

namespace TopMovies.DAL
{
    public interface IMovieRepository
    {
        Genre? AddOrUpdateGenre(IGenre? genre);
        Movie AddOrUpdateMovie(IMovie movie);
        IEnumerable<MovieGenre>? AddOrUpdateMovieGenres(Movie? movie, IEnumerable<Genre>? genres);
        Person? AddOrUpdatePerson(IPerson? person);
        TopMovieHistory AddTopMovies(IEnumerable<IMovie> movies);
        void Save();
    }
}