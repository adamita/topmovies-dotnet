﻿using System;
using System.Collections.Generic;

namespace TopMovies.DAL.Model
{
    public partial class Movie
    {
        public Movie()
        {
            MovieGenres = new HashSet<MovieGenre>();
            TopMovies = new HashSet<TopMovie>();
        }

        public long Id { get; set; }
        public string Title { get; set; }
        public string Overview { get; set; }
        public string Url { get; set; }
        public int? Length { get; set; }
        public string PosterUrl { get; set; }
        public decimal? VoteVarage { get; set; }
        public DateTime? ReleaseDate { get; set; }
        public long? DirectorId { get; set; }
        public DateTime Created { get; set; }
        public DateTime Updated { get; set; }
        public bool Removed { get; set; }

        public virtual Person Director { get; set; }
        public virtual ICollection<MovieGenre> MovieGenres { get; set; }
        public virtual ICollection<TopMovie> TopMovies { get; set; }
    }
}
