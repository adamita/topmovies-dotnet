﻿using System;
using System.Collections.Generic;

namespace TopMovies.DAL.Model
{
    public partial class Genre
    {
        public Genre()
        {
            MovieGenres = new HashSet<MovieGenre>();
        }

        public long Id { get; set; }
        public string Name { get; set; }
        public DateTime Created { get; set; }
        public DateTime Updated { get; set; }
        public bool Removed { get; set; }

        public virtual ICollection<MovieGenre> MovieGenres { get; set; }
    }
}
