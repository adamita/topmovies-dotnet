﻿using System;
using System.Collections.Generic;

namespace TopMovies.DAL.Model
{
    public partial class TopMovie
    {
        public long Id { get; set; }
        public long HistoryId { get; set; }
        public long MovieId { get; set; }
        public int Order { get; set; }
        public bool Removed { get; set; }

        public virtual TopMovieHistory History { get; set; }
        public virtual Movie Movie { get; set; }
    }
}
