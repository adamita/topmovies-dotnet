﻿using System;
using System.Collections.Generic;

namespace TopMovies.DAL.Model
{
    public partial class Person
    {
        public Person()
        {
            Movies = new HashSet<Movie>();
        }

        public long Id { get; set; }
        public string Name { get; set; }
        public string Bio { get; set; }
        public DateTime? Birthday { get; set; }
        public DateTime Created { get; set; }
        public DateTime Updated { get; set; }
        public bool Removed { get; set; }

        public virtual ICollection<Movie> Movies { get; set; }
    }
}
