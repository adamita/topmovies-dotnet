﻿using System;
using System.Collections.Generic;

namespace TopMovies.DAL.Model
{
    public partial class TopMovieHistory
    {
        public TopMovieHistory()
        {
            TopMovies = new HashSet<TopMovie>();
        }

        public long Id { get; set; }
        public DateTime? Date { get; set; }
        public bool Removed { get; set; }

        public virtual ICollection<TopMovie> TopMovies { get; set; }
    }
}
