﻿using System;
using System.Collections.Generic;

namespace TopMovies.DAL.Model
{
    public partial class MovieGenre
    {
        public long Id { get; set; }
        public long MovieId { get; set; }
        public long GenreId { get; set; }
        public bool Removed { get; set; }

        public virtual Genre Genre { get; set; }
        public virtual Movie Movie { get; set; }
    }
}
