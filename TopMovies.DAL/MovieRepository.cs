﻿using TopMovies.DAL.Model;
using TopMovies.Interfaces;

namespace TopMovies.DAL
{
    public class MovieRepository : IMovieRepository
    {
        private TopMoviesContext context;

        public MovieRepository(TopMoviesContext context)
        {
            this.context = context;
        }

        public Genre? AddOrUpdateGenre(IGenre? genre)
        {
            if (genre == null) return null;

            var rec = context.Genres.FirstOrDefault(x => x.Id == genre.Id);
            if (rec == null)
            {
                rec = context.Genres.Add(new Genre()
                {
                    Id = genre.Id,
                    Name = genre.Name,
                }).Entity;
            }
            else if (rec.Name != genre.Name)
            {
                rec.Name = genre.Name;
                rec.Updated = DateTime.Now;
            }

            Save();
            return rec;
        }

        public Person? AddOrUpdatePerson(IPerson? person)
        {
            if (person == null) return null;

            var rec = context.People.FirstOrDefault(x => x.Id == person.Id);
            if (rec == null)
            {
                rec = context.People.Add(new Person()
                {
                    Id = person.Id,
                    Name = person.Name,
                    Bio = person.Bio,
                    Birthday = person.Birthday
                }).Entity;
            }
            else if (rec.Name != person.Name || rec.Bio != person.Bio || rec.Birthday != person.Birthday)
            {
                rec.Name = person.Name;
                rec.Bio = person.Bio;
                rec.Birthday = person.Birthday;
                rec.Updated = DateTime.Now;
            }

            Save();
            return rec;
        }

        public Movie AddOrUpdateMovie(IMovie movie)
        {
            var director = AddOrUpdatePerson(movie.Director);
            var genres = movie.Genres.Select(x => AddOrUpdateGenre(x));
            var rec = context.Movies.FirstOrDefault(x => x.Id == movie.Id);
            if (rec == null)
            {
                rec = context.Movies.Add(new Movie()
                {
                    Id = movie.Id,
                    Title = movie.Title,
                    DirectorId = director?.Id,
                    ReleaseDate = movie.ReleaseDate,
                    Length = movie.Length,
                    Overview = movie.Overview,
                    PosterUrl = movie.Poster_url,
                    Url = movie.Url,
                    VoteVarage = (decimal?)movie.Vote_average,
                }).Entity;
            }
            else if (rec.Title != movie.Title)
            {
                rec.Title = movie.Title;
                rec.DirectorId = director?.Id;
                rec.ReleaseDate = movie.ReleaseDate;
                rec.Length = movie.Length;
                rec.Overview = movie.Overview;
                rec.PosterUrl = movie.Poster_url;
                rec.Url = movie.Url;
                rec.VoteVarage = (decimal?)movie.Vote_average;
                rec.Updated = DateTime.Now;
            }
            Save();
            AddOrUpdateMovieGenres(rec, genres);
            return rec;
        }

        public IEnumerable<MovieGenre>? AddOrUpdateMovieGenres(Movie? movie, IEnumerable<Genre>? genres)
        {
            if (movie == null || genres == null) return null;

            var moviegenres = context.MovieGenres.Where(x => x.MovieId == movie.Id && !x.Removed).ToList();

            var remove = moviegenres.Where(x => !genres.Any(y => y.Id == x.GenreId));
            var add = genres.Where(x => !moviegenres.Any(y => y.GenreId == x.Id));

            foreach (var item in remove)
            {
                item.Removed = true;
            }

            context.MovieGenres.AddRange(add.Select(x => new MovieGenre()
            {
                MovieId = movie.Id,
                GenreId = x.Id
            }));
            Save();
            return context.MovieGenres.Where(x => x.MovieId == movie.Id && !x.Removed).ToList();
        }

        public TopMovieHistory AddTopMovies(IEnumerable<IMovie> movies)
        {
            var list = movies.Select(x => AddOrUpdateMovie(x));
            var tops = context.TopMovieHistories.Add(new TopMovieHistory()
            {
                Date = DateTime.Now,
            }).Entity;
            Save();
            
            context.TopMovies.AddRange(list.Select((y, i) => new TopMovie()
            {
                HistoryId=tops.Id,
                MovieId = y.Id,
                Order = i
            }
            ));
            Save();
            return tops;
        }

        public void Save()
        {
            context.SaveChanges();
        }
    }
}
