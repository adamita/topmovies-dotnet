﻿
namespace TopMovies.Interfaces
{
    public interface IMovie
    {
        IPerson? Director { get; set; }
        IEnumerable<IGenre> Genres { get; set; }
        long Id { get; set; }
        int? Length { get; set; }
        string? Overview { get; set; }
        string? Poster_url { get; set; }
        DateTime? ReleaseDate { get; set; }
        string? Title { get; set; }
        string? Url { get; set; }
        double? Vote_average { get; set; }
        int? Vote_count { get; set; }
    }
}