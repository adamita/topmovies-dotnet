﻿
namespace TopMovies.Interfaces
{
    public interface IPerson
    {
        long Id { get; set; }
        string? Bio { get; set; }
        DateTime? Birthday { get; set; }
        string? Name { get; set; }
    }
}