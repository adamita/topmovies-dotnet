﻿namespace TopMovies.Interfaces
{
    public interface IGenre
    {
        long Id { get; set; }
        string? Name { get; set; }
    }
}