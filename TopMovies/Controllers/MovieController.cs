using Microsoft.AspNetCore.Mvc;
using TopMovies.BLL;
using TopMovies.BLL.TMDBService;
using TopMovies.DAL;

namespace TopMovies.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class MovieController : ControllerBase
    {
        private readonly ILogger<MovieController> _logger;
        private readonly IMovieRepository _repo;
        private readonly ITMDBRepository _tmdbRepo;

        public MovieController(ILogger<MovieController> logger, IMovieRepository repo, ITMDBRepository tMDBRepository)
        {
            _logger = logger;
            _repo= repo;
            _tmdbRepo = tMDBRepository;
        }

        [HttpGet(Name = "GetTopMovies")]
        public async Task<bool> GetAsync(int count)
        {
            string key = "948a10deabf10e845decad102c913e9a";

            if (count < 1)
                return false;

            var httpClient = new HttpClient();
            var client = new TMDBClient(httpClient);
            var list = await _tmdbRepo.GetTopRatedMovies(count, key, client);
            var movies = await _tmdbRepo.GetMovieDetails(key, client, list);
            _repo.AddTopMovies(movies);

            return movies.Count==count;
        }
    }
}